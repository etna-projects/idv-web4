package com.quest.etna;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
public class DefaultControllerTest {
    @Autowired
	private MockMvc mockMvc;

    @Test
    public void testSuccess() throws Exception {
        this.mockMvc
                .perform(get("/testSuccess"))
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    public void testNotFound() throws Exception {
        this.mockMvc
                .perform(get("/testNotFound"))
                .andDo(print())
                .andExpect(status().isNotFound());
    }

    @Test
    public void testError() throws Exception {
            this.mockMvc
                    .perform(get("/testError"))
                    .andDo(print())
                    .andExpect(status().isInternalServerError());
    }
}
