package com.quest.etna;

import com.github.javafaker.Faker;
import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import com.quest.etna.repositories.UserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    private Faker faker = new Faker(new Locale("en-GB"));

    private static final HashMap<String,String> normalUser = new HashMap<String,String>(){{
        put("username", "test");
        put("password", "test");
    }};
    
    private static final HashMap<String,String> adminUser = new HashMap<String,String>(){{
        put("username", "test2");
        put("password", "test");
    }};

    private int userId;
    private int adminId;

    private String userToken;
    private String adminToken;

    @BeforeAll
    public void setup() throws Exception {
        User user1 = userRepository.findByUsername(normalUser.get("username"));
        User user2 = userRepository.findByUsername(adminUser.get("username"));
        if (user1 != null) {
            userRepository.delete(user1);
        }
        if (user2 != null) {
            userRepository.delete(user2);
        }
        this.mockMvc
                .perform(
                        post("/register")
                                .contentType("application/json")
                                .content(new JSONObject(normalUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isCreated());
        String response = this.mockMvc
                .perform(
                        post("/authenticate")
                                .contentType("application/json")
                                .content(new JSONObject(normalUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("token"));
        userToken = jsonResponse.getString("token");
        user1 = userRepository.findByUsername(normalUser.get("username"));
        userId = user1.getId();

        this.mockMvc
                .perform(
                        post("/register")
                                .contentType("application/json")
                                .content(new JSONObject(adminUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isCreated());
        response = this.mockMvc
                .perform(
                        post("/authenticate")
                                .contentType("application/json")
                                .content(new JSONObject(adminUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("token"));
        adminToken = jsonResponse.getString("token");
        user2 = userRepository.findByUsername(adminUser.get("username"));
        user2.setRole(UserRole.ROLE_ADMIN);
        userRepository.save(user2);
        adminId = user2.getId();
    }

    @AfterAll
    public void cleanup() {
        User user1 = userRepository.findByUsername(normalUser.get("username"));
        User user2 = userRepository.findByUsername(adminUser.get("username"));
        if (user1 != null) {
            userRepository.delete(user1);
        }
        if (user2 != null) {
            userRepository.delete(user2);
        }
    }

    @Test
    @Order(1)
    @DisplayName("sans token Bearer, la route /user/ retourne bien un statut 401.")
    void getUsersShouldReturn401WithoutToken() throws Exception {
        this.mockMvc
                .perform(
                        get("/user")
                                .contentType("application/json")
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Order(2)
    @DisplayName("avec un token Bearer valide, la route /user/ retourne bien un statut 200.")
    void getUsersShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        get("/user")
                                .contentType("application/json")
                                .header("Authorization", "Bearer " + userToken)
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONArray jsonResponse = new JSONArray(response);
        assertTrue(jsonResponse.length() > 0);
        for(int i = 0; i < jsonResponse.length(); i++) {
            JSONObject user = jsonResponse.getJSONObject(i);
            assertTrue(user.has("id"));
            assertTrue(user.has("username"));
            assertTrue(user.has("role"));
            assertFalse(user.has("password"));
            JSONArray addresses = user.getJSONArray("address");
            if(addresses.length() > 0){
                for(int j = 0; j < addresses.length(); j++) {
                    JSONObject address = addresses.getJSONObject(j);
                    assertTrue(address.has("id"));
                    assertTrue(address.has("street"));
                    assertTrue(address.has("city"));
                    assertTrue(address.has("country"));
                    assertTrue(address.has("postalCode"));
                    assertFalse(address.has("user"));
                }
            }
        }
    }

    @Test
    @Order(3)
    @DisplayName("La route GET /user/:id répond bien en 200.")
    void getUserShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        get("/user/"+userId)
                                .contentType("application/json")
                                .accept("application/json")
                                .header("Authorization", "Bearer " + userToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("username"));
        assertEquals(normalUser.get("username"), jsonResponse.getString("username"));
        assertTrue(jsonResponse.has("role"));
        assertEquals("ROLE_USER", jsonResponse.getString("role"));
        assertFalse(jsonResponse.has("password"));
    }

    @Test
    @Order(4)
    @DisplayName("La route PUT /user/:id répond bien en 403 quand un user qui n'est pas admin veut en update un autre.")
    void updateUserByIdShouldReturn403() throws Exception {
        HashMap<String,String> updatedUser = new HashMap<>();
        updatedUser.put("username", "test2updated");
        this.mockMvc
                .perform(
                        put("/user/" + adminId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + userToken)
                                .content(new JSONObject(updatedUser).toString())
                )
                .andDo(print())
                .andExpect(status().isForbidden());
    }

    @Test
    @Order(5)
    @DisplayName("La route PUT /user/:id répond bien en 200 et retourne bien le user à jour.")
    void updateUserByIdShouldReturn200() throws Exception {
        HashMap<String,String> updatedUser = new HashMap<>();
        updatedUser.put("username", "test1updated");
        String response = this.mockMvc
                .perform(
                        put("/user/" + userId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + adminToken)
                                .content(new JSONObject(updatedUser).toString())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("username"));
        assertEquals(updatedUser.get("username"), jsonResponse.getString("username"));
        assertTrue(jsonResponse.has("role"));
        assertEquals("ROLE_USER", jsonResponse.getString("role"));
        // revert changes
        updatedUser.put("username", normalUser.get("username"));
        this.mockMvc
                .perform(
                        put("/user/" + userId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + adminToken)
                                .content(new JSONObject(updatedUser).toString())
                )
                .andDo(print())
                .andExpect(status().isOk());
    }

    @Test
    @Order(6)
    @DisplayName("La route DETELE /user/:id répond bien en 403 quand un user qui n'est pas admin veut en delete un autre.")
    void deleteUserByIdShouldReturn403() throws Exception {
        this.mockMvc
                .perform(
                        delete("/user/" + adminId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + userToken)
                )
                .andDo(print())
                .andExpect(status().isForbidden());
        // On vérifie que l'user n'a pas été supprimé
        User user = userRepository.findById(adminId).orElse(null);
        assertNotNull(user);
        assertEquals(user.getRole(), UserRole.ROLE_ADMIN);
        assertEquals(user.getUsername(), adminUser.get("username"));
    }

    @Test
    @Order(7)
    @DisplayName("La route DELETE /user/:id répond bien en 200")
    void deleteUserByIdShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        delete("/user/" + userId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + adminToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("success"));
        if(jsonResponse.getBoolean("success")) {
            // On vérifie que l'user a bien été supprimée en base de données
            Optional<User> dbUser = userRepository.findById(userId);
            assertFalse(dbUser.isPresent());
        } else {
            fail("La suppression de l'adresse a échoué.");
        }
    }

    @Test
    @Order(8)
    @DisplayName("La route /user/ répond bien en 200 et retourne bien les users et leur adresses mais pas de user dans les adresses")
    void getUsersShouldSendUsersWithAddresses() throws Exception {
        HashMap<String,String> testAddress = new HashMap<String,String>(){{
        put("street", faker.address().streetAddress());
        put("city", faker.address().city());
        put("postalCode", faker.address().zipCode());
        put("country", faker.address().country());
    }};
        this.mockMvc
                .perform(
                        post("/address")
                                .contentType("application/json")
                                .accept("application/json")
                                .header("Authorization", "Bearer " + adminToken)
                                .content(new JSONObject(testAddress).toString())
                )
                .andDo(print())
                .andExpect(status().isCreated());
        String response = this.mockMvc
                .perform(
                        get("/user")
                                .contentType("application/json")
                                .header("Authorization", "Bearer " + adminToken)
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONArray jsonResponse = new JSONArray(response);
        assertTrue(jsonResponse.length() > 0);
        for(int i = 0; i < jsonResponse.length(); i++) {
            JSONObject user = jsonResponse.getJSONObject(i);
            assertTrue(user.has("username"));
            assertTrue(user.has("role"));
            assertTrue(user.has("address"));
            JSONArray addresses = user.getJSONArray("address");
            assertTrue(addresses.length() > 0);
            for(int j = 0; j < addresses.length(); j++) {
                JSONObject address = addresses.getJSONObject(j);
                assertTrue(address.has("id"));
                assertTrue(address.has("street"));
                assertTrue(address.has("city"));
                assertTrue(address.has("postalCode"));
                assertFalse(address.has("user"));
            }
        }
    }
}
