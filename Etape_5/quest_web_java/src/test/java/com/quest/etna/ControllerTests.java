package com.quest.etna;

import org.junit.jupiter.api.Test;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;

@Suite
@SelectClasses({
        DefaultControllerTest.class,
        AuthenticationControllerTest.class,
        AddressControllerTest.class,
        UserControllerTest.class
})
public class ControllerTests {
    @Test
    public void testAuthenticate() throws Exception {
        // La route /register répond bien en 201.
        // Si vous rappelez /register avec les mêmes paramètres, vous obtenez bien une réponse 409 car l'utilisateur existe déjà.
        // La route /authenticate retourne bien un statut 200 ainsi que votre token.
        // La route /me retourne un statut 200 avec les informations de l'utilisateur (attention à bien penser au token Bearer).
    }

    @Test
    public void testUser() throws Exception {
        // Sans token Bearer, la route /user retourne bien un statut 401
        // Avec un token Bearer valide, la route /user retourne bien un statut 200.
        // Avec un ROLE_USER, la suppression retourne bien un statut 403.
        // Avec un ROLE_ADMIN, la suppression retourne bien un statut 200.
    }

    @Test
    public void testAddress() throws Exception {
        // Sans token Bearer, la route /address/ retourne bien un statut 401.
        // Avec un token Bearer valide, la route /address/ retourne bien un statut 200.
        // Avec un token Bearer valide, l'ajout d'une adresse retourne bien un statut 201.
        // Avec un ROLE_USER, la suppression d'une adresse qui n'est pas la sienne retourne bien un statut 403.
        // Avec un ROLE_ADMIN, la suppression d'une adresse qui n'est pas la sienne retourne bien un statut 200.
    }
}
