package com.quest.etna.dto;

import com.quest.etna.model.UserRole;
import com.quest.etna.utils.EnumNamePattern;

import javax.validation.constraints.Size;
import java.util.Objects;

public class UpdateUserBodyDto {
    public UpdateUserBodyDto() {
    }
    public UpdateUserBodyDto(String username, UserRole role) {
        this.username = username;
        this.role = role;
    }

    @Size(min = 1, max = 255, message = "Username must be between 1 and 255 characters")
    private String username;

    @EnumNamePattern(regexp = "ROLE_(USER|ADMIN)", message = "Role must be either ROLE_USER or ROLE_ADMIN")
    private UserRole role;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public UserRole getRole() {
        return role;
    }

    public void setRole(UserRole role) {
        this.role = role;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UpdateUserBodyDto)) return false;
        UpdateUserBodyDto that = (UpdateUserBodyDto) o;
        return Objects.equals(username, that.username) && role == that.role;
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, role);
    }

    @Override
    public String toString() {
        return "UpdateUserBody{" +
                "username='" + username + '\'' +
                ", role=" + role +
                '}';
    }
}
