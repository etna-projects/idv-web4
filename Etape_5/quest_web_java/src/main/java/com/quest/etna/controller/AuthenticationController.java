package com.quest.etna.controller;

import com.quest.etna.config.JwtTokenUtil;
import com.quest.etna.config.JwtUserDetailsService;
import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import com.quest.etna.model.UserDetails;
import com.quest.etna.model.UserRole;
import com.quest.etna.repositories.UserRepository;

import com.quest.etna.dto.AuthenticateUserBodyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.HashMap;

@RestController
public class AuthenticationController {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtTokenUtil jwtTokenUtil;

    @Autowired
    private JwtUserDetailsService userDetailsService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @PostMapping("/register")
    public ResponseEntity<?> register(
            @Valid @RequestBody AuthenticateUserBodyDto body
    ) {
        String username = body.getUsername();
        String password = body.getPassword();


        User existingUser = userRepository.findByUsername(username);

        if (existingUser != null) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Le nom d'utilisateur existe déjà.");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
        }

        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(passwordEncoder.encode(password));
        newUser.setRole(UserRole.ROLE_USER);

        userRepository.save(newUser);

        HashMap<String, String> userDetails = new HashMap<>();
        userDetails.put("username", newUser.getUsername());
        userDetails.put("role", newUser.getRole().toString());

        return ResponseEntity.status(HttpStatus.CREATED).body(userDetails);
    }

    @GetMapping("/me")
    public ResponseEntity<?> me() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User user = userRepository.findByUsername(jwtUserDetails.getUsername());
        HashMap<String, String> response = new HashMap<>();
        if (user == null) {
            response.put("error", "Impossible de récupérer les informations de l'utilisateur.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(response);
        }
        response.put("username", user.getUsername());
        response.put("role", user.getRole().toString());
        response.put("id", user.getId().toString());
        return ResponseEntity.status(HttpStatus.OK).body(response);
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> createAuthenticationToken(
            @Valid @RequestBody AuthenticateUserBodyDto body
    ) {
        String username = body.getUsername();
        String password = body.getPassword();

        try {
            authenticate(username, password);
        } catch (Exception e) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Nom d'utilisateur ou mot de passe invalide.");
            return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(error);
        }

        final JwtUserDetails userDetails = userDetailsService
                .loadUserByUsername(username);

        final String token = jwtTokenUtil.generateToken(userDetails);

        HashMap<String, String> responseBody = new HashMap<>();
        responseBody.put("token", token);

        return ResponseEntity.status(HttpStatus.OK).body(responseBody);
    }

    private void authenticate(String username, String password) throws Exception {
        try {
            authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
        } catch (DisabledException e) {
            throw new Exception("USER_DISABLED", e);
        } catch (BadCredentialsException e) {
            throw new Exception("INVALID_CREDENTIALS", e);
        }
    }

}
