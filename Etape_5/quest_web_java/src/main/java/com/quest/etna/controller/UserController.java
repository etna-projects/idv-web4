package com.quest.etna.controller;

import com.quest.etna.exceptions.HttpException;
import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import com.quest.etna.repositories.UserRepository;
import com.quest.etna.dto.UpdateUserBodyDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.Collection;
import java.util.HashMap;
import java.util.Objects;

@RestController
@RequestMapping("user")
@Validated
public class UserController {
    @Autowired
    private UserRepository userRepository;

    @GetMapping("")
    public ResponseEntity<?> getAllUsers() {
        Iterable<User> users = userRepository.findAll();
        return ResponseEntity.ok(users);
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getUserById(
            @PathVariable("id") @Min(1) int id
    ) throws HttpException {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw HttpException.notFound("User not found");
        }
        return ResponseEntity.ok(user);
    }

    @PutMapping("{id}")
    public ResponseEntity<?> updateUserById(
            HttpServletRequest request,
            @PathVariable("id") @Min(1) int id,
            @Valid @RequestBody UpdateUserBodyDto updatedUser
    ) throws HttpException {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw HttpException.notFound("Utilisateur non trouvé");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User authUser = userRepository.findByUsername(jwtUserDetails.getUsername());
        if (!Objects.equals(authUser.getId(), id) && !request.isUserInRole("ROLE_ADMIN")) {
            throw HttpException.forbidden("Vous n'êtes pas autorisé à modifier cet utilisateur.");
        }
        User exitingUserWithSameUsername = userRepository.findByUsername(updatedUser.getUsername());
        if (exitingUserWithSameUsername != null && exitingUserWithSameUsername.getId() != id) {
            throw HttpException.conflict("Le nom d'utilisateur existe déjà.");
        }
        // since updatedUser fields are nullable, we only update the fields that are not null
        if (updatedUser.getUsername() != null) {
            user.setUsername(updatedUser.getUsername());
        }
        if (updatedUser.getRole() != null) {
            user.setRole(updatedUser.getRole());
        }
        userRepository.save(user);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteUserById(
            HttpServletRequest request,
            @PathVariable("id") @Min(1) int id
    ) throws HttpException {
        User user = userRepository.findById(id).orElse(null);
        if (user == null) {
            throw HttpException.notFound("User not found");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User authUser = userRepository.findByUsername(jwtUserDetails.getUsername());
        if (!Objects.equals(authUser.getId(), id) && !request.isUserInRole("ROLE_ADMIN")) {
            throw HttpException.forbidden("You are not allowed to delete this user");
        }
        HashMap<String, Boolean> response = new HashMap<>();
        try {
            userRepository.deleteById(id);
        } catch (Exception e) {
            response.put("success", false);
            return ResponseEntity.internalServerError().body(response);
        }
        response.put("success", true);
        return ResponseEntity.ok(response);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException e) {
        HashMap<String, String> errors = new HashMap<>();
        e.getConstraintViolations().forEach(constraintViolation -> {
            String propertyPath = constraintViolation.getPropertyPath().toString();
            String message = constraintViolation.getMessage();
            errors.put(propertyPath, message);
        });
        HashMap<String, HashMap<String, String>> response = new HashMap<>();
        response.put("errors", errors);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpException.class)
    ResponseEntity<?> handleException(HttpException e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("error", e.getMessage());
        return new ResponseEntity<>(response, e.getStatus());
    }
}
