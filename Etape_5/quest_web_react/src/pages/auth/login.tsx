import {
  Anchor,
  Button,
  Container,
  LoadingOverlay,
  Paper,
  PasswordInput,
  Text,
  TextInput,
  Title,
} from '@mantine/core';
import { useState } from 'react';
import { useForm } from '@mantine/form';
import { LoginSchema } from '@/utils/validators/auth.yup';
import { Asserts } from 'yup';
import { showErrorNotification, showSuccessNotification } from '@/utils/notification';
import { useRouter } from 'next/router';
import useAuthStore from '@/store/auth.store';

type FormValues = Asserts<typeof LoginSchema>;

export default function Login() {
  const performLogin = useAuthStore((state) => state.login);
  const getCurrenUser = useAuthStore((state) => state.getMe);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const form = useForm<FormValues>({
    initialValues: {
      username: '',
      password: '',
    },
    validateInputOnBlur: true,
  });

  const handleSubmit = async (values: FormValues) => {
    try {
      setLoading(true);
      await performLogin(values);
      await getCurrenUser();
      showSuccessNotification({
        title: 'Connexion réussie',
        message: 'Vous allez être redirigé vers l\'application',
      });
      const redirectTo = router.query.to as string | undefined;
      await router.push(redirectTo || '/');
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container size={420} my={40}>
      <Title
        align="center"
        sx={{ fontWeight: 900 }}
      >
        Bon retour parmi nous !
      </Title>
      <Text color="dimmed" size="sm" align="center" mt={5}>
        Vous n'avez pas encore de compte ?
        {' '}
        <Anchor<'a'> size="sm" href="/auth/register">
          Créer un compte
        </Anchor>
      </Text>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Paper
          withBorder
          shadow="md"
          p={30}
          mt={30}
          radius="md"
          sx={{ position: 'relative' }}
        >
          <LoadingOverlay visible={loading} />
          <TextInput
            label="Nom d'utilisateur"
            placeholder="jdoe"
            required
            {...form.getInputProps('username')}
          />
          <PasswordInput
            label="Mot de passe"
            placeholder="Votre mot de passe"
            required
            mt="md"
            {...form.getInputProps('password')}
          />
          <Button fullWidth mt="xl" type="submit">
            Se connecter
          </Button>
        </Paper>
      </form>
    </Container>
  );
}
