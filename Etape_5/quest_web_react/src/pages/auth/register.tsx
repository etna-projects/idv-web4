import {
  Anchor,
  Button,
  Container,
  LoadingOverlay,
  Paper,
  PasswordInput,
  Text,
  TextInput,
  Title,
} from '@mantine/core';
import { useState } from 'react';
import { useForm } from '@mantine/form';
import { RegisterSchema } from '@/utils/validators/auth.yup';
import { Asserts } from 'yup';
import { showErrorNotification, showSuccessNotification } from '@/utils/notification';
import { useRouter } from 'next/router';
import useAuthStore from '@/store/auth.store';

type FormValues = Asserts<typeof RegisterSchema>;

export default function Register() {
  const performRegister = useAuthStore((state) => state.register);
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const form = useForm<FormValues>({
    initialValues: {
      username: '',
      password: '',
      passwordConfirm: '',
    },
    validateInputOnBlur: true,
  });

  const handleSubmit = async (values: FormValues) => {
    try {
      setLoading(true);
      await performRegister(values);
      showSuccessNotification({
        title: 'Votre compte a été créé avec succès',
        message: 'Vous pouvez maintenant vous connecter',
      });
      await router.push('/auth/login');
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <Container size={420} my={40}>
      <Title
        align="center"
        sx={{ fontWeight: 900 }}
      >
        Bienvenue parmi nous !
      </Title>
      <Text color="dimmed" size="sm" align="center" mt={5}>
        Vous avez déjà un compte ?
        {' '}
        <Anchor<'a'> size="sm" href="/auth/login">
          Connectez-vous
        </Anchor>
      </Text>
      <form onSubmit={form.onSubmit(handleSubmit)}>
        <Paper
          withBorder
          shadow="md"
          p={30}
          mt={30}
          radius="md"
          sx={{ position: 'relative' }}
        >
          <LoadingOverlay visible={loading} />
          <TextInput
            label="Nom d'utilisateur"
            placeholder="jdoe"
            required
            {...form.getInputProps('username')}
          />
          <PasswordInput
            label="Mot de passe"
            placeholder="Votre mot de passe"
            required
            mt="md"
            {...form.getInputProps('password')}
          />
          <PasswordInput
            label="Confirmer le mot de passe"
            placeholder="Votre mot de passe"
            required
            mt="md"
            {...form.getInputProps('passwordConfirm')}
          />
          <Button fullWidth mt="xl" type="submit">
            Créer un compte
          </Button>
        </Paper>
      </form>
    </Container>
  );
}
