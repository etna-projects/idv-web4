import { AppProps } from 'next/app';
import Head from 'next/head';
import { MantineProvider } from '@mantine/core';
import theme from '@/config/theme';
import { Notifications } from '@mantine/notifications';
import RouterTransition from '@/components/utilities/RouterTransition';
import { ModalsProvider } from '@mantine/modals';
import modals from '@/config/modals';
import setupDayJs from '@/utils/dayjs';

setupDayJs();

export default function App(props: AppProps) {
  const { Component, pageProps } = props;

  return (
    <>
      <Head>
        <title>Quest Web</title>
        <meta name="viewport" content="minimum-scale=1, initial-scale=1, width=device-width" />
      </Head>
      <MantineProvider
        withGlobalStyles
        withNormalizeCSS
        theme={theme}
      >
        <Notifications position="top-right" />
        <ModalsProvider
          modals={modals}
          modalProps={{
            centered: true,
          }}
        >
          <RouterTransition />
          <Component {...pageProps} />
        </ModalsProvider>
      </MantineProvider>
    </>
  );
}
