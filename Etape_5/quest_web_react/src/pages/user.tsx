import AppShell from '@/components/layout/AppShell';
import { useUserListStore } from '@/store/userList.store';
import { useCallback, useEffect, useState } from 'react';
import { showErrorNotification, showSuccessNotification } from '@/utils/notification';
import {
  ActionIcon, Box, Group, Title,
} from '@mantine/core';
import dayjs from 'dayjs';
import { openConfirmModal, openContextModal } from '@mantine/modals';
import { IconEdit, IconTrash } from '@tabler/icons-react';
import { DataTable } from 'mantine-datatable';
import useAuthStore from '@/store/auth.store';
import { UpdateUserModalName } from '@/components/modals/UpdateUserModal';
import UserAddressExpansion from '@/components/UserAddressExpansion';

const RowExpansion = (isAdmin: boolean) => ({ record: r }: {
  record: User;
}) => (
  <UserAddressExpansion
    address={r?.address || []}
    isAdmin={isAdmin}
  />
);

const UsersPage = () => {
  const [loading, setLoading] = useState(true);
  const isAdmin = useAuthStore((state) => state.role === 'ROLE_ADMIN');
  const users = useUserListStore((state) => state.users || []);
  const getAllUsers = useUserListStore((state) => state.getAll);
  const deleteById = useUserListStore((state) => state.deleteUser);

  const fetchUsers = useCallback(async () => {
    try {
      setLoading(true);
      await getAllUsers();
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  }, [getAllUsers]);

  const performDelete = useCallback(async (userId: number) => {
    try {
      setLoading(true);
      await deleteById(userId);
      showSuccessNotification({
        title: 'Utilisateur supprimé',
        message: 'Utilisateur supprimé avec succès',
      });
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  }, []);

  useEffect(() => {
    fetchUsers();
  }, [fetchUsers]);

  return (
    <AppShell loading={loading}>
      <Box p="md">
        <Group mt="xl" mb="sm">
          <Title order={3}>Liste des utilisateurs</Title>
        </Group>
        <DataTable
          records={users}
          minHeight={150}
          noRecordsText="Aucun utilisateur retrouvé"
          rowExpansion={{
            content: RowExpansion(isAdmin),
          }}
          columns={[
            {
              title: '#',
              accessor: 'id',
              width: 30,
            },
            {
              title: 'Nom d\'utilisateur',
              accessor: 'username',
            },
            {
              title: 'Rôle',
              accessor: 'role',
              render: (d) => (d.role === 'ROLE_ADMIN' ? 'Administrateur' : 'Utilisateur'),
            },
            {
              title: 'Date de creation',
              accessor: 'creationDate',
              width: 150,
              render: (d) => dayjs(d.creationDate).format('LLL'),
            },
            {
              title: 'Date de modification',
              accessor: 'updatedDate',
              width: 150,
              render: ({ updatedDate }) => dayjs(updatedDate).format('LLL'),
            },
            {
              title: 'Actions',
              accessor: 'id',
              width: 100,
              hidden: !isAdmin,
              render: ({ id }) => (
                <Group>
                  <ActionIcon
                    color="blue"
                    variant="filled"
                    title="Modifier l'utilisateur"
                    onClick={() => openContextModal({
                      title: 'Mise à jour de l\'utilisateur',
                      modal: UpdateUserModalName,
                      innerProps: {
                        user_id: id,
                        onFinish: fetchUsers,
                      },
                    })}
                  >
                    <IconEdit size="1.125rem" />
                  </ActionIcon>
                  <ActionIcon
                    color="red"
                    variant="filled"
                    title={"Supprimer l'utilisateur"}
                    onClick={() => openConfirmModal({
                      title: 'Suppression de l\'utilisateur',
                      children: 'Êtes-vous sûr de vouloir vous supprimer cet utilisateur ? Cette action est irréversible.',
                      labels: { confirm: 'Supprimer', cancel: 'Annuler' },
                      confirmProps: { color: 'red' },
                      onConfirm: () => id && performDelete(id),
                    })}
                  >
                    <IconTrash size="1.125rem" />
                  </ActionIcon>
                </Group>
              ),
            },
          ]}
        />
      </Box>
    </AppShell>
  );
};
export default UsersPage;
