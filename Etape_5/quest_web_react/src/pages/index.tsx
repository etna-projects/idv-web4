import AppShell from '@/components/layout/AppShell';
import useAuthStore from '@/store/auth.store';
import { useCallback, useEffect, useState } from 'react';
import { showErrorNotification } from '@/utils/notification';
import {
  ActionIcon,
  Avatar, Box, Group, Stack, Text, Title,
} from '@mantine/core';
import dayjs from 'dayjs';
import { capitalize } from 'radash';
import Result from '@/components/utilities/Result';
import { DataTable } from 'mantine-datatable';
import { useShownUserStore } from '@/store/shownUser.store';
import { IconEdit, IconPlus, IconTrash } from '@tabler/icons-react';
import { openConfirmModal, openContextModal } from '@mantine/modals';
import { UpdateAddressModalName } from '@/components/modals/UpdateAddressModal';
import { CreateAddressModalName } from '@/components/modals/CreateAddressModal';

const HomePage = () => {
  const user_id = useAuthStore((state) => state.id);
  const [loading, setLoading] = useState(true);
  const username = useShownUserStore((state) => state.username);
  const role = useShownUserStore((state) => state.role);
  const creationDate = useShownUserStore((state) => state.creationDate);
  const address = useShownUserStore((state) => state.address);
  const getUserById = useShownUserStore((state) => state.getById);
  const deleteAddress = useShownUserStore((state) => state.deleteAddress);

  const fetchUser = useCallback(async (userId: number) => {
    try {
      setLoading(true);
      await getUserById(userId);
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  }, [getUserById]);

  useEffect(() => {
    if (user_id) fetchUser(user_id);
  }, [fetchUser, user_id]);

  if (!username || !role || !creationDate) {
    return (
      <AppShell loading={loading}>
        <Result
          type="warning"
          title="Informations manquantes"
          message="Les informations de l'utilidsateur sont manquantes"
        />
      </AppShell>
    );
  }

  return (
    <AppShell loading={loading}>
      <Box p="md">
        <Group>
          <Avatar radius="xl" size="xl">
            {username?.split(' ')?.map((n) => n[0]?.toUpperCase())?.join('')}
          </Avatar>
          <Stack spacing={0}>
            <Title order={2}>{capitalize(username)}</Title>
            <Text color="dimmed">{role === 'ROLE_ADMIN' ? 'Administrateur' : 'Utilisateur'}</Text>
            <Text color="dimmed" size="xs">
              {`Crée le ${dayjs(creationDate).format('LLL')}`}
            </Text>
          </Stack>
        </Group>
        <Group mt="xl" mb="sm">
          <Title order={3}>Mes addresses</Title>
          <ActionIcon
            color="blue"
            variant="filled"
            title="Créer une addresse"
            onClick={() => openContextModal({
              title: 'Créer une addresse',
              modal: CreateAddressModalName,
              innerProps: {
                onFinish: () => fetchUser(user_id as number),
              },
            })}
          >
            <IconPlus size="1.125rem" />
          </ActionIcon>
        </Group>
        <DataTable
          records={address}
          minHeight={150}
          noRecordsText="Aucune addresse retrouvée"
          columns={[
            {
              title: '#',
              accessor: 'id',
              width: 30,
            },
            {
              title: 'N° et rue',
              accessor: 'street',
            },
            {
              title: 'Ville',
              accessor: 'city',
              render: ({ city }) => capitalize(city || ''),
            },
            {
              title: 'Code postal',
              accessor: 'postalCode',
              width: 110,
            },
            {
              title: 'Pays',
              accessor: 'country',
              render: ({ country }) => capitalize(country || ''),
            },
            {
              title: 'Date de creation',
              accessor: 'creationDate',
              width: 150,
              render: (d) => dayjs(d.creationDate).format('LLL'),
            },
            {
              title: 'Date de modification',
              accessor: 'updatedDate',
              width: 150,
              render: ({ updatedDate }) => dayjs(updatedDate).format('LLL'),
            },
            {
              title: 'Actions',
              accessor: 'id',
              width: 100,
              render: ({ id }) => (
                <Group>
                  <ActionIcon
                    color="blue"
                    variant="filled"
                    title="Modifier l'addresse"
                    onClick={() => openContextModal({
                      title: 'Mise à jour de l\'addresse',
                      modal: UpdateAddressModalName,
                      innerProps: {
                        address_id: id as number,
                        onFinish: () => fetchUser(user_id as number),
                      },
                    })}
                  >
                    <IconEdit size="1.125rem" />
                  </ActionIcon>
                  <ActionIcon
                    color="red"
                    variant="filled"
                    title={"Supprimer l'addresse"}
                    onClick={() => openConfirmModal({
                      title: 'Suppression de l\'addresse',
                      children: 'Êtes-vous sûr de vouloir vous supprimer cette addresse de votre compte ? Cette action est irréversible.',
                      labels: { confirm: 'Supprimer', cancel: 'Annuler' },
                      confirmProps: { color: 'red' },
                      onConfirm: () => id && deleteAddress(id),
                    })}
                  >
                    <IconTrash size="1.125rem" />
                  </ActionIcon>
                </Group>
              ),
            },
          ]}
        />
      </Box>
    </AppShell>
  );
};
export default HomePage;
