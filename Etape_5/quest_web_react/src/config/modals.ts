import CreateAddressModal, { CreateAddressModalName } from '@/components/modals/CreateAddressModal';
import UpdateAddressModal, { UpdateAddressModalName } from '@/components/modals/UpdateAddressModal';
import UpdateUserModal, { UpdateUserModalName } from '@/components/modals/UpdateUserModal';

const modals = {
  [CreateAddressModalName]: CreateAddressModal,
  [UpdateAddressModalName]: UpdateAddressModal,
  [UpdateUserModalName]: UpdateUserModal,
};

declare module '@mantine/modals' {
  export interface MantineModalsOverride {
    modals: typeof modals;
  }
}

export default modals;
