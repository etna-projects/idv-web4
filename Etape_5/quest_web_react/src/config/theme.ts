import { MantineThemeOverride } from '@mantine/core';

const theme: MantineThemeOverride = {
  datesLocale: 'en',
  colorScheme: 'dark',
};

export default theme;
