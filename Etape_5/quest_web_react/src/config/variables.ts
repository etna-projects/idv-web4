import getConfig from 'next/config';

const { publicRuntimeConfig } = getConfig();

const APP_ENV = publicRuntimeConfig.APP_ENV || 'production';

const dev = !['production', 'staging'].includes(APP_ENV);

const cookies = {
  jwt: 'x-jwt',
};

const app = {
  defaultPageSize: 50,
};

const api = {
  url: publicRuntimeConfig.API_URL,
};

const variables = {
  APP_ENV,
  dev,
  cookies,
  app,
  api,
};

export default variables;
