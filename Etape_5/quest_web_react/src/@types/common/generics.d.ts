type Nullable<T> = T | null;

type DeepNullable<T> = {
  [K in keyof T]: T[K] extends Array<infer R> ? Array<DeepNullable<R>> : DeepNullable<T[K]> | null;
};

type NonFunctionPropertyNames<T> = { [K in keyof T]: T[K] extends Function ? never : K }[keyof T];

type NonFunctionProperties<T> = Pick<T, NonFunctionPropertyNames<T>>;
