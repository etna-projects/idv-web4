interface Address {
  id: number;
  street: string;
  postalCode: string;
  city: string;
  country: string;
  creationDate: number;
  updatedDate: number;
}
