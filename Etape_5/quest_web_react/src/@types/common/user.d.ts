type UserRole = 'ROLE_USER' | 'ROLE_ADMIN';

interface User {
  id: number;
  username: string;
  role: string;
  address: Address[];
  creationDate: number;
  updatedDate: number;
}
