declare namespace UserHttp {
  interface DeleteResponse {
    success: boolean;
  }
  interface UpdatePayload {
    username?: string;
    role?: UserRole;
  }

  type UpdateResponse = User;

  type GetAllResponse = User[];

  type GetByIdResponse = User;
}
