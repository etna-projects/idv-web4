declare namespace AuthHttp {
  interface LoginPayload {
    username: string;
    password: string;
  }

  interface RegisterPayload {
    username: string;
    password: string;
  }

  interface LoginResponse {
    token: string;
  }

  interface RegisterResponse {
    username: string;
    role: UserRole;
  }

  interface MeResponse {
    username: string;
    role: UserRole;
    id: number;
  }
}
