declare namespace AddressHttp {
  interface DeleteResponse {
    success: boolean
  }

  interface CreatePayload {
    street: string;
    postalCode: string;
    city: string;
    country: string;
  }

  interface UpdatePayload {
    street?: string;
    postalCode?: string;
    city?: string;
    country?: string;
  }

  type GetByIdResponse = Address;

  type GetAllResponse = Address[];

  type CreateResponse = Address;

  type UpdateResponse = Address;
}
