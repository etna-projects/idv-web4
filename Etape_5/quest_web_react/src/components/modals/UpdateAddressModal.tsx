import { ContextModalProps } from '@mantine/modals';
import { useCallback, useEffect, useState } from 'react';
import { UpdateAddressSchema } from '@/utils/validators/address.yup';
import { Asserts } from 'yup';
import { useForm } from '@mantine/form';
import api from '@/api';
import { showErrorNotification, showSuccessNotification } from '@/utils/notification';
import {
  Box, Button, Group, LoadingOverlay, TextInput,
} from '@mantine/core';

interface UpdateAddressModalProps {
  onFinish?: () => void | Promise<void>;
  address_id: number;
}

type FormValues = Asserts<typeof UpdateAddressSchema>;
export default function UpdateAddressModal({
  id,
  context,
  innerProps: {
    onFinish = () => {},
    address_id,
  },
}: ContextModalProps<UpdateAddressModalProps>) {
  const [loading, setLoading] = useState(false);
  const {
    getInputProps,
    isValid,
    setValues,
    onSubmit,
  } = useForm<FormValues>({
    initialValues: {
      street: '',
      city: '',
      country: '',
      postalCode: '',
    },
  });

  const fetchAddress = useCallback(async (addressId: number) => {
    try {
      setLoading(true);
      const { data } = await api.address.get(addressId);
      setValues({
        street: data.street,
        city: data.city,
        country: data.country,
        postalCode: data.postalCode,
      });
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  }, [setValues]);

  useEffect(() => {
    fetchAddress(address_id);
  }, [fetchAddress, address_id]);

  const handleSubmit = async (values: FormValues) => {
    try {
      setLoading(true);
      await api.address.update(address_id, {
        city: values.city as string,
        country: values.country as string,
        postalCode: values.postalCode as string,
        street: values.street as string,
      });
      await onFinish();
      showSuccessNotification({
        title: 'Addresse modifiée',
        message: "L'addresse a bien été modifiée",
      });
      context.closeModal(id);
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <form onSubmit={onSubmit(handleSubmit)}>
      <Box sx={{ position: 'relative' }}>
        <LoadingOverlay visible={loading} />
        <TextInput
          label="N° et rue"
          {...getInputProps('street')}
        />
        <Group mt="md">
          <TextInput
            sx={{ flex: 3 }}
            label="Ville"
            {...getInputProps('city')}
          />
          <TextInput
            sx={{ flex: 1 }}
            label="Code postal"
            {...getInputProps('postalCode')}
          />
        </Group>
        <TextInput
          mt="md"
          label="Pays"
          {...getInputProps('country')}
        />
        <Group position="apart" mt="xl">
          <Button mt="md" color="blue" onClick={() => context.closeModal(id)} disabled={loading}>
            Abandonner
          </Button>
          <Button
            mt="md"
            color="green"
            type="submit"
            disabled={loading || !isValid()}
          >
            Confirmer
          </Button>
        </Group>

      </Box>
    </form>
  );
}

export const UpdateAddressModalName = 'UpdateAddressModal';
