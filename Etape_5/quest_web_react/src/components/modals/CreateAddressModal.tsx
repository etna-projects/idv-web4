import { ContextModalProps } from '@mantine/modals';
import { useState } from 'react';
import { useForm } from '@mantine/form';
import api from '@/api';
import { showErrorNotification, showSuccessNotification } from '@/utils/notification';
import {
  Box, Button, Group, LoadingOverlay, TextInput,
} from '@mantine/core';
import { Asserts } from 'yup';
import { CreateAddressSchema } from '@/utils/validators/address.yup';

interface CreateAddressModalProps {
  onFinish?: () => void | Promise<void>;
}
type FormValues = Asserts<typeof CreateAddressSchema>;
export default function CreateAddressModal({
  id,
  context,
  innerProps: {
    onFinish = () => {},
  },
}: ContextModalProps<CreateAddressModalProps>) {
  const [loading, setLoading] = useState(false);
  const {
    getInputProps,
    isValid,
    onSubmit,
  } = useForm<FormValues>({
    initialValues: {
      street: '',
      city: '',
      country: '',
      postalCode: '',
    },
  });

  const handleSubmit = async (values: FormValues) => {
    try {
      setLoading(true);
      await api.address.create({
        city: values.city as string,
        country: values.country as string,
        postalCode: values.postalCode as string,
        street: values.street as string,
      });
      await onFinish();
      showSuccessNotification({
        title: 'Addresse crée',
        message: "L'addresse a bien été crée",
      });
      context.closeModal(id);
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <form onSubmit={onSubmit(handleSubmit)}>
      <Box sx={{ position: 'relative' }}>
        <LoadingOverlay visible={loading} />
        <TextInput
          label="N° et rue"
          {...getInputProps('street')}
        />
        <Group mt="md">
          <TextInput
            sx={{ flex: 3 }}
            label="Ville"
            {...getInputProps('city')}
          />
          <TextInput
            sx={{ flex: 1 }}
            label="Code postal"
            {...getInputProps('postalCode')}
          />
        </Group>
        <TextInput
          mt="md"
          label="Pays"
          {...getInputProps('country')}
        />
        <Group position="apart" mt="xl">
          <Button mt="md" color="blue" onClick={() => context.closeModal(id)} disabled={loading}>
            Abandonner
          </Button>
          <Button
            mt="md"
            color="green"
            type="submit"
            disabled={loading || !isValid()}
          >
            Confirmer
          </Button>
        </Group>

      </Box>
    </form>
  );
}

export const CreateAddressModalName = 'CreateAddressModal';
