import { ContextModalProps } from '@mantine/modals';
import { useCallback, useEffect, useState } from 'react';
import { showErrorNotification, showSuccessNotification } from '@/utils/notification';
import api from '@/api';
import { Asserts } from 'yup';
import { UpdateUserSchema } from '@/utils/validators/user.yup';
import { useForm } from '@mantine/form';
import {
  Box, Button, Group, LoadingOverlay, Select, TextInput,
} from '@mantine/core';
import { roleOptions } from '@/utils/commonSelectOptions';

interface UpdateUserModalProps {
  user_id: number;
  onFinish?: () => void | Promise<void>;
}

type FormValues = Asserts<typeof UpdateUserSchema>;
export default function UpdateUserModal({
  id,
  context,
  innerProps: {
    user_id,
    onFinish = () => {},
  },
}: ContextModalProps<UpdateUserModalProps>) {
  const [loading, setLoading] = useState(false);

  const {
    getInputProps,
    isValid,
    setValues,
    onSubmit,
  } = useForm<FormValues>({
    initialValues: {
      username: '',
      role: null,
    },
  });

  const fetchUser = useCallback(async (userId: number) => {
    try {
      setLoading(true);
      const { data } = await api.user.get(userId);
      setValues({
        username: data.username,
        role: data.role as UserRole,
      });
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  }, [setValues]);

  useEffect(() => {
    fetchUser(user_id);
  }, [fetchUser, user_id]);

  const handleSubmit = async (values: FormValues) => {
    try {
      setLoading(true);
      await api.user.update(user_id, {
        username: values.username as string | undefined,
        role: values.role as UserRole | undefined,
      });
      await onFinish();
      showSuccessNotification({
        title: 'Utilisateur modifié',
        message: "L'utilisateur a bien été modifié",
      });
      context.closeModal(id);
    } catch (error) {
      showErrorNotification(error);
    } finally {
      setLoading(false);
    }
  };

  return (
    <form onSubmit={onSubmit(handleSubmit)}>
      <Box sx={{ position: 'relative' }}>
        <LoadingOverlay visible={loading} />
        <TextInput
          label="Nom d'utilisateur"
          placeholder="jdoe"
          {...getInputProps('username')}
        />
        <Select
          mt="md"
          label="Rôle"
          placeholder="Pick one"
          data={roleOptions}
          {...getInputProps('role')}
        />
        <Group position="apart" mt="xl">
          <Button mt="md" color="blue" onClick={() => context.closeModal(id)} disabled={loading}>
            Abandonner
          </Button>
          <Button
            mt="md"
            color="green"
            type="submit"
            disabled={loading || !isValid()}
          >
            Confirmer
          </Button>
        </Group>

      </Box>
    </form>
  );
}

export const UpdateUserModalName = 'UpdateUserModal';
