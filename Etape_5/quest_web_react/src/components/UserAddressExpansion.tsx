import { capitalize } from 'radash';
import dayjs from 'dayjs';
import {
  ActionIcon, Box, Group, Title,
} from '@mantine/core';
import { openConfirmModal, openContextModal } from '@mantine/modals';
import { UpdateAddressModalName } from '@/components/modals/UpdateAddressModal';
import { IconEdit, IconTrash } from '@tabler/icons-react';
import { DataTable } from 'mantine-datatable';
import { useUserListStore } from '@/store/userList.store';

interface UserAddressExpansionProps {
  address: Address[];
  isAdmin: boolean;
}

export default function UserAddressExpansion({ address, isAdmin }: UserAddressExpansionProps) {
  const getAllAddresses = useUserListStore((state) => state.getAll);
  return (
    <Box p="md">
      <Title order={6}>Liste des addresses de l'utilisateur</Title>
      <DataTable
        records={address}
        minHeight={150}
        noRecordsText="Aucune addresse retrouvée"
        columns={[
          {
            title: '#',
            accessor: 'id',
            width: 30,
          },
          {
            title: 'N° et rue',
            accessor: 'street',
          },
          {
            title: 'Ville',
            accessor: 'city',
            render: ({ city }) => capitalize(city || ''),
          },
          {
            title: 'Code postal',
            accessor: 'postalCode',
            width: 110,
          },
          {
            title: 'Pays',
            accessor: 'country',
            render: ({ country }) => capitalize(country || ''),
          },
          {
            title: 'Date de creation',
            accessor: 'creationDate',
            width: 150,
            render: (d) => dayjs(d.creationDate).format('LLL'),
          },
          {
            title: 'Date de modification',
            accessor: 'updatedDate',
            width: 150,
            render: ({ updatedDate }) => dayjs(updatedDate).format('LLL'),
          },
          {
            title: 'Actions',
            accessor: 'id',
            hidden: !isAdmin,
            width: 100,
            render: ({ id }) => (
              <Group>
                <ActionIcon
                  color="blue"
                  variant="filled"
                  title="Modifier l'addresse"
                  onClick={() => openContextModal({
                    title: 'Mise à jour de l\'addresse',
                    modal: UpdateAddressModalName,
                    innerProps: {
                      address_id: id as number,
                      onFinish: getAllAddresses,
                    },
                  })}
                >
                  <IconEdit size="1.125rem" />
                </ActionIcon>
                <ActionIcon
                  color="red"
                  variant="filled"
                  title={"Supprimer l'addresse"}
                  onClick={() => openConfirmModal({
                    title: 'Suppression de l\'addresse',
                    children: 'Êtes-vous sûr de vouloir vous supprimer cette addresse de votre compte ? Cette action est irréversible.',
                    labels: { confirm: 'Supprimer', cancel: 'Annuler' },
                    confirmProps: { color: 'red' },
                    onConfirm: getAllAddresses,
                  })}
                >
                  <IconTrash size="1.125rem" />
                </ActionIcon>
              </Group>
            ),
          },
        ]}
      />
    </Box>
  );
}
