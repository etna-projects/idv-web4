import {
  Center, Loader, AppShell as MantineAppShell,
  useMantineTheme,
} from '@mantine/core';
import Header from '@/components/layout/Header';
import NavbarNested from '@/components/layout/Navbar';
import { IconUser, IconUsers } from '@tabler/icons-react';

const headerLinks: HeaderLink[] = [
  { label: 'Mon profil', link: '/' },
  { label: 'Utilisateurs', link: '/user' },
];

const navbarLinks: NavLink[] = [
  {
    label: 'Mon profil', link: '/', description: 'Mes informations de profil', icon: <IconUser />,
  },
  {
    label: 'Utilisateurs', link: '/user', description: 'Liste des utilisateurs', icon: <IconUsers />,
  },
];

interface AppShellProps {
  children?: React.ReactNode;
  loading?: boolean;
}

export default function AppShell({ children, loading }: AppShellProps) {
  const theme = useMantineTheme();
  return (
    <MantineAppShell
      styles={{
        main: {
          background: theme.colorScheme === 'dark' ? theme.colors.dark[8] : theme.colors.gray[0],
        },
      }}
      navbarOffsetBreakpoint="sm"
      asideOffsetBreakpoint="sm"
      header={<Header links={headerLinks} />}
      navbar={<NavbarNested links={navbarLinks} />}
    >
      {loading ? <Center><Loader size="xl" variant="dots" /></Center> : children}
    </MantineAppShell>
  );
}
