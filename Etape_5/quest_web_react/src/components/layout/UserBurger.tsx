import { forwardRef, useEffect } from 'react';
import {
  IconChevronDown, IconChevronUp,
  IconDoorExit, IconEdit, IconTrash,
} from '@tabler/icons-react';
import {
  Avatar, Group, MediaQuery, Menu, Text, UnstyledButton,
} from '@mantine/core';
import useAuthStore from '@/store/auth.store';
import { capitalize } from 'radash';
import { openConfirmModal, openContextModal } from '@mantine/modals';
import { UpdateUserModalName } from '@/components/modals/UpdateUserModal';
import useAppStore from '@/store/app.store';

interface UserButtonProps extends React.ComponentPropsWithoutRef<'button'> {
  username: string;
  role: UserRole;
}

const UserButton = forwardRef<HTMLButtonElement, UserButtonProps>(
  ({
    username, role, ...others
  }: UserButtonProps, ref) => (
    <UnstyledButton
      ref={ref}
      sx={(theme) => ({
        color: theme.colorScheme === 'dark' ? theme.colors.dark[0] : theme.black,
        padding: theme.spacing.xs,
      })}
      {...others}
    >
      <Group>
        <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
          <IconChevronUp size="1rem" />
        </MediaQuery>
        <MediaQuery smallerThan="sm" styles={{ display: 'none' }}>
          <Avatar radius="xl">{username?.split(' ')?.map((n) => n[0]?.toUpperCase())?.join('')}</Avatar>
        </MediaQuery>
        <div style={{ flex: 1 }}>
          <Text size="sm" weight={500}>
            {capitalize(username)}
          </Text>

          <Text color="dimmed" size="xs">
            {role === 'ROLE_ADMIN' ? 'Administrateur' : 'Utilisateur'}
          </Text>
        </div>
        <MediaQuery largerThan="sm" styles={{ display: 'none' }}>
          <Avatar radius="xl">{username?.split(' ')?.map((n) => n[0]?.toUpperCase())?.join('')}</Avatar>
        </MediaQuery>
        <MediaQuery smallerThan="sm" styles={{ display: 'none' }}>
          <IconChevronDown size="1rem" />
        </MediaQuery>
      </Group>
    </UnstyledButton>
  ),
);

export default function UserBurger() {
  const username = useAuthStore((state) => state.username);
  const role = useAuthStore((state) => state.role);
  const user_id = useAuthStore((state) => state.id);
  const logout = useAuthStore((state) => state.logout);
  const deleteAccount = useAuthStore((state) => state.deleteMe);
  const getCurrentUser = useAuthStore((state) => state.getMe);
  const setNavbarOpen = useAppStore((state) => state.setNavbarOpen);

  useEffect(() => {
    getCurrentUser();
  }, [getCurrentUser, username]);
  return (
    <Menu withArrow withinPortal>
      <Menu.Target>
        <UserButton
          username={username || ''}
          role={role || 'ROLE_USER'}
        />
      </Menu.Target>
      <Menu.Dropdown>
        <Menu.Item
          color="blue"
          disabled={!user_id}
          icon={<IconEdit size="0.9rem" stroke={1.5} />}
          onClick={() => {
            if (!user_id) return;
            setNavbarOpen(false);
            openContextModal({
              title: 'Mettre à jour mon profil',
              modal: UpdateUserModalName,
              innerProps: {
                user_id,
                onFinish: getCurrentUser,
              },
            });
          }}
        >
          Mettre à jour mon profil
        </Menu.Item>
        <Menu.Item
          color="red"
          icon={<IconDoorExit size="0.9rem" stroke={1.5} />}
          onClick={() => {
            setNavbarOpen(false);
            openConfirmModal({
              title: 'Déconnexion',
              children: 'Êtes-vous sûr de vouloir vous déconnecter ?',
              labels: { confirm: 'Déconnexion', cancel: 'Annuler' },
              confirmProps: { color: 'red' },
              onConfirm: logout,
            });
          }}
        >
          Déconnexion
        </Menu.Item>
        <Menu.Divider />
        <Menu.Label>Zone de danger</Menu.Label>
        <Menu.Item
          color="red"
          icon={<IconTrash size="0.9rem" stroke={1.5} />}
          onClick={() => {
            setNavbarOpen(false);
            openConfirmModal({
              title: 'Supprimer mon compte',
              children: 'Êtes-vous sûr de vouloir vous supprimer votre compte ? Cette action est irréversible.',
              labels: { confirm: 'Supprimer', cancel: 'Annuler' },
              confirmProps: { color: 'red' },
              onConfirm: deleteAccount,
            });
          }}
        >
          Supprimer mon compte
        </Menu.Item>
      </Menu.Dropdown>
    </Menu>
  );
}
