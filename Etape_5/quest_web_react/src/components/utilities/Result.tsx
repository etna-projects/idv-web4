import {
  Center,
  Container,
  Stack,
  Text,
  Title,
  createStyles,
  useMantineTheme,
} from '@mantine/core';
import {
  IconAlertCircle,
  IconAlertTriangle,
  IconBarrierBlock,
  IconCircleCheck,
  IconCircleX,
} from '@tabler/icons-react';

interface ResultProps {
  type: 'success' | 'error' | 'warning' | 'info' | 'building'
  title: string;
  message: string;
  children?: React.ReactNode[];
}

const ICON_SIZE = 64;

const useStyles = createStyles(() => ({
  root: {
    paddingTop: 80,
    paddingBottom: 80,
  },

  inner: {
    position: 'relative',
  },
}));

function Result({
  type, title, message, children,
}:ResultProps) {
  const { classes } = useStyles();
  const theme = useMantineTheme();
  return (
    <Container className={classes.root}>
      <div className={classes.inner}>
        <Center>
          <Stack align="center">
            {type === 'success' && (<IconCircleCheck color={theme.colors.green[5]} size={ICON_SIZE} />)}
            {type === 'error' && (<IconCircleX color={theme.colors.red[5]} size={ICON_SIZE} />)}
            {type === 'warning' && (<IconAlertTriangle color={theme.colors.orange[5]} size={ICON_SIZE} />)}
            {type === 'info' && (<IconAlertCircle color={theme.colors.blue[5]} size={ICON_SIZE} />)}
            {type === 'building' && (<IconBarrierBlock color={theme.colors.blue[5]} size={ICON_SIZE} />)}
            <Title align="center">{title}</Title>
            <Text align="center">{message}</Text>
            {children}
          </Stack>
        </Center>
      </div>
    </Container>
  );
}

export default Result;
