import * as yup from 'yup';

export const UpdateUserSchema = yup.object().shape({
  username: yup.string().notRequired().min(2).max(255),
  role: yup.string().notRequired().oneOf(['ROLE_USER', 'ROLE_ADMIN']).notRequired(),
});
