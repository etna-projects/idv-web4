import * as yup from 'yup';

export const CreateAddressSchema = yup.object().shape({
  street: yup.string().min(1).max(100).required(),
  postalCode: yup.string().min(1).max(30).required(),
  city: yup.string().min(1).max(50).required(),
  country: yup.string().min(1).max(50).required(),
});

export const UpdateAddressSchema = yup.object().shape({
  street: yup.string().notRequired().min(1).max(100),
  postalCode: yup.string().notRequired().min(1).max(30),
  city: yup.string().notRequired().min(1).max(50),
  country: yup.string().notRequired().min(1).max(50),
});
