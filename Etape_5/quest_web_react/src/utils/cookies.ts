import variables from '@/config/variables';
import { deleteCookie, getCookie, setCookie } from 'cookies-next';
import dayjs from 'dayjs';

export const saveApiToken = async (token: string, expiration?: string) => {
  setCookie(
    variables.cookies.jwt,
    token,
    {
      expires: expiration ? dayjs(expiration).toDate() : undefined,
    },
  );
};

export const removeApiToken = () => {
  deleteCookie(variables.cookies.jwt);
};

export const getApiToken = () => getCookie(
  variables.cookies.jwt,
)?.toString();
