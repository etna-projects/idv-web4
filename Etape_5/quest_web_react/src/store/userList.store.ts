import { create } from 'zustand';
import api from '@/api';

interface UserListStore {
  users: User[];
  getAll: () => Promise<void>;
  deleteUser: (userId: number) => Promise<void>;
}

const initialState: NonFunctionProperties<UserListStore> = {
  users: [],
};

export const useUserListStore = create<UserListStore>((set, get) => ({
  ...initialState,
  getAll: async () => {
    const { data } = await api.user.list();
    set({ users: data });
  },
  deleteUser: async (userId: number) => {
    await api.user.delete(userId);
    const { users } = get();
    set({ users: users.filter((user) => user.id !== userId) });
  },
}));
