import { create } from 'zustand';

interface AppState {
  isNavbarOpen: boolean;
  toggleNavbar: () => void;
  setNavbarOpen: (value: boolean) => void;
}

const useAppStore = create<AppState>((set, get) => ({
  isNavbarOpen: false,
  toggleNavbar: () => set({ isNavbarOpen: !get().isNavbarOpen }),
  setNavbarOpen: (value) => set({ isNavbarOpen: value }),
}));

export default useAppStore;
