import { create } from 'zustand';
import api from '@/api';

interface ShownUserStore extends DeepNullable<User> {
  getById: (id: number) => Promise<void>;
  deleteAddress: (addressId: number) => Promise<void>;
}

const initialState: DeepNullable<User> = {
  id: null,
  username: null,
  role: null,
  address: [],
  creationDate: null,
  updatedDate: null,
};

export const useShownUserStore = create<ShownUserStore>((set, get) => ({
  ...initialState,
  getById: async (id: number) => {
    const { data } = await api.user.get(id);
    set(data);
  },
  deleteAddress: async (addressId: number) => {
    await api.address.delete(addressId);
    const { address } = get();
    if (!address) return;
    set({ address: address.filter((a) => a?.id !== addressId) });
  },
}));
