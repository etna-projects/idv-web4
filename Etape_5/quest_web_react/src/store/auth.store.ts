import { create } from 'zustand';
import api from '@/api';
import { removeApiToken, saveApiToken } from '@/utils/cookies';

interface AuthState {
  id: Nullable<number>;
  username: Nullable<string>;
  role: Nullable<UserRole>;
  login: (
    data: { username: string, password: string }
  ) => Promise<void>;
  register: (data: { username: string, password: string }) => Promise<void>;
  logout: () => Promise<void>;
  getMe: () => Promise<void>;
  deleteMe: () => Promise<void>;
}

const initialState: NonFunctionProperties<AuthState> = {
  id: null,
  username: null,
  role: null,
};

const useAuthStore = create<AuthState>((set, get) => ({
  ...initialState,
  login: async (data) => {
    const res = await api.auth.login(data);
    await saveApiToken(res.data.token);
  },
  register: async (data) => {
    await api.auth.register(data);
  },
  logout: async () => {
    await removeApiToken();
    set(initialState);
  },
  getMe: async () => {
    const res = await api.auth.me();
    set({ username: res.data.username, role: res.data.role, id: res.data.id });
  },
  deleteMe: async () => {
    if (!get().id) throw new Error('L\'utilisateur n\'est pas connecté');
    const { data: { success } } = await api.user.delete(get().id as number);
    if (!success) throw new Error('Une erreur est survenue lors de la suppression de votre compte');
    await get().logout();
  },
}));

export default useAuthStore;
