import axios, { AxiosInstance } from 'axios';
import variables from '@/config/variables';
import Router from 'next/router';
import { getApiToken } from '@/utils/cookies';
import { showWarningNotification } from '@/utils/notification';

class Api {
  private readonly api: AxiosInstance;

  constructor(
    baseURL: string = variables.api.url,
  ) {
    this.api = axios.create({
      baseURL,
      responseType: 'json',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        'cache-control': 'no-cache',
      },
    });
    this.api.interceptors.request.use((config) => {
      const token = getApiToken();
      if (!token) {
        return config;
      }
      const authorization = config.headers.get('Authorization');
      if (authorization) {
        return config;
      }
      config.headers.set('Authorization', `Bearer ${token}`);
      return config;
    });
    this.api.interceptors.response.use(
      (response) => response,
      (error) => {
        if (!error.response?.status) {
          return Promise.reject(error);
        }
        if (error.config.url.includes('/authenticate') && error.config.method === 'post') {
          return Promise.reject(error);
        }
        if (error.response?.status === 401) {
          showWarningNotification({
            id: 'auth-required',
            title: 'Authentification',
            message: 'Vous devez être authentifié pour accéder à cette page.',
          });
          return Router.push('/auth/login');
        }
        return Promise.reject(error);
      },
    );
  }

  auth = {
    login: (body: AuthHttp.LoginPayload) => this.api.post<AuthHttp.LoginResponse>('/authenticate', body),
    register: (body: AuthHttp.RegisterPayload) => this.api.post<AuthHttp.RegisterResponse>('/register', body),
    me: (token?: string) => this.api.get<AuthHttp.MeResponse>(
      '/me',
      {
        headers: {
          Authorization: token ? `Bearer ${token}` : undefined,
        },
      },
    ),
  };

  user = {
    list: () => this.api.get<UserHttp.GetAllResponse>('/user'),
    update: (id: number, body: UserHttp.UpdatePayload) => this.api.put<UserHttp.UpdateResponse>(`/user/${id}`, body),
    delete: (id: number) => this.api.delete<UserHttp.DeleteResponse>(`/user/${id}`),
    get: (id: number) => this.api.get<UserHttp.GetByIdResponse>(`/user/${id}`),
  };

  address = {
    list: () => this.api.get<AddressHttp.GetAllResponse>('/address'),
    update: (id: number, body: AddressHttp.UpdatePayload) => this.api.put<AddressHttp.UpdateResponse>(`/address/${id}`, body),
    delete: (id: number) => this.api.delete<AddressHttp.DeleteResponse>(`/address/${id}`),
    get: (id: number) => this.api.get<AddressHttp.GetByIdResponse>(`/address/${id}`),
    create: (body: AddressHttp.CreatePayload) => this.api.post<AddressHttp.CreateResponse>('/address', body),
  };
}

const api = new Api();

export default api;
