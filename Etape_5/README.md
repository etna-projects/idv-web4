# IDV-WEB4

## Prérequis
- [Nodejs 18.16.0](https://nodejs.org/en/download) Vous pouvez utiliser [Volta](https://volta.sh) pour gérer les versions de NodeJS
- [JDK 11](https://adoptium.net/temurin/releases/?version=11) pour lancer le serveur
- [Mysql 5.7](https://dev.mysql.com/downloads/mysql/5.7.html) pour la base de données

## Configuration
Frontend
- APP_ENV: `production` ou `development` defaut: `production`
- API_URL: URL de l'API defaut: `http://localhost:8090` (Requis)

Backend
- server.port: port du serveur. défaut: `8090`
- spring.datasource.url: URL de la base de données. defaut: `jdbc:mysql://localhost:3306/quest_web?useUnicode=true&characterEncoding=utf8&useSSL=false&serverTimezone=UTC`
- spring.datasource.username: username de la base de données. defaut: `application`
- spring.datasource.password: mot de passe de la base de données. defaut: `password`
- jwt.secret: secret pour creer les tokens JWT. defaut:`etna_quest`

## Compilation
Pour le backend
```bash
cd Etape_5/quest_web_java
# Modifier les variables si besoin
# nano src/main/resources/application.properties
chmod +x mvnw && ./mvnw clean package -DskipTests
```
Le binaire est disponible à ce chemin `Etape_5/quest_web_java/target/quest_web-0.0.1-SNAPSHOT.jar`

Pour le frontend
```bash
# Installer node 18.16.0 si vous utilisez volta
# volta install node@18.16.0
# Installer yarn si nous n'utilisez pas volta
# npm install -g yarn@1.22.19
# Installer les dependances
cd Etape_5/quest_web_react
yarn install
# Mettre en place les variables d'environnement
cp example.env .env.local
# Modifier les variables d'environnement si besoin
# nano .env.local
# Compiler le projet
yarn build
```

## Lancement en production
```bash
# Lancer le backend
cd Etape_5/quest_web_java && java -jar target/quest_web-0.0.1-SNAPSHOT.jar
```
L'API est disponible sur le port configuré dans le fichier `application.properties` par défaut `8090`.
```bash
# Lancer le frontend
cd Etape_5/quest_web_react && yarn start
```
L'app est disponible sur le port configuré dans le fichier `.env.local` par défaut `3000`.
Il est aussi possible d'obtenir le port dans la console apres le lancement du frontend.

## Lancement en développement
```bash
# Lancer le backend
# Modifier les variables si besoin
# nano src/main/resources/application.properties
cd Etape_5/quest_web_java && ./mvnw spring-boot:run
```
L'API est disponible sur le port configuré dans le fichier `application.properties` par défaut `8090`.
```bash
# Lancer le frontend
# Mettre en place les variables d'environnement
cp example.env .env.local
# Modifier les variables d'environnement si besoin
# nano .env.local
cd Etape_5/quest_web_react && yarn dev
```
L'app est disponible sur le port configuré dans le fichier `.env.local` par défaut `3000`.
Il est aussi possible d'obtenir le port dans la console apres le lancement du frontend.

## Lancement avec docker en mode production (incompatible avec les puces ARM)
```bash
cd Etape_5
docker compose up -d --build
```
L'API est disponible sur le port `8090`.
L'app est disponible sur le port `8091`.
