package com.etna.sportevents.models;

public enum UserRole {
    ROLE_USER,
    ROLE_MODERATOR,
    ROLE_ADMIN,
}
