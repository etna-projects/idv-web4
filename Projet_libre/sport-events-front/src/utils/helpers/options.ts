export const roleOptions = [
  { value: 'ROLE_ADMIN', label: 'Administrateur' },
  { value: 'ROLE_MODERATOR', label: 'Modérateur' },
  { value: 'ROLE_USER', label: 'Utilisateur' },
];
