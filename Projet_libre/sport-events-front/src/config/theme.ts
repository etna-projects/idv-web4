import { MantineThemeOverride } from '@mantine/core';

const theme: MantineThemeOverride = {
  datesLocale: 'fr',
  colorScheme: 'dark',
};

export default theme;
