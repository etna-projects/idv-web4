# Projet Événements Sportifs

Ce projet est une plateforme permettant aux utilisateurs de créer, partager et participer à des événements sportifs dans leur région. Les utilisateurs peuvent s'inscrire, rejoindre des événements, laisser des notes et des commentaires sur les événements auxquels ils ont participé.

## Fonctionnalités

1. **Authentification et autorisation** : Les utilisateurs peuvent s'inscrire et se connecter à la plateforme. Les utilisateurs peuvent avoir l'un des rôles suivants : ROLE_USER, ROLE_ADMIN.
2. **Création d'événements sportifs** : Les utilisateurs peuvent créer des événements sportifs en précisant le sport, la date, l'heure, le lieu, le nombre de places disponibles et une description.
3. **Recherche et participation aux événements** : Les utilisateurs peuvent rechercher des événements sportifs en fonction de leur sport préféré, de leur localisation ou de la date. Ils peuvent s'inscrire à des événements en fonction des places disponibles.
4. **Notes et commentaires** : Après avoir participé à un événement, les utilisateurs peuvent laisser une note et un commentaire sur l'événement et l'organisateur.
5. **Géolocalisation et cartographie** : Les événements sont affichés sur une carte interactive, permettant aux utilisateurs de visualiser facilement les événements à proximité de leur localisation.
6. **Espace administrateur** : Les utilisateurs ayant le rôle ROLE_ADMIN ont accès à un espace administrateur leur permettant de gérer les événements, les utilisateurs et les rôles.
7. **CRUD événements et commentaires** : Les utilisateurs ayant le rôle ROLE_USER peuvent créer, lire, mettre à jour et supprimer leurs événements et commentaires respectifs.

## Structure de la base de données

La base de données comprend les tables suivantes :

1. `roles` : stocke les différents rôles d'utilisateur (ROLE_USER et ROLE_ADMIN).
2. `users` : stocke les informations sur les utilisateurs, y compris leurs rôles.
3. `sports` : stocke les différents sports disponibles sur la plateforme.
4. `events` : stocke les informations sur les événements sportifs, y compris l'organisateur, le sport associé, la date, l'heure, la localisation et le nombre de places disponibles.
5. `registrations` : stocke les inscriptions des utilisateurs aux événements sportifs, y compris la date d'inscription.
6. `ratings` : stocke les notes attribuées par les utilisateurs aux événements auxquels ils ont participé, ainsi qu'à l'organisateur de l'événement.
7. `comments` : stocke les commentaires laissés par les utilisateurs pour les événements auxquels ils ont participé.

Les tables sont reliées entre elles à l'aide de clés relationnelles.

## Technologies utilisées

- Java pour le développement de l'API
- ReactJS pour le développement de l'application front-end
- MySQL pour la gestion de la base de données
