package com.quest.etna.controller;

import com.quest.etna.model.User;
import com.quest.etna.model.UserRole;
import com.quest.etna.model.UserDetails;
import com.quest.etna.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class AuthenticationController {

    @Autowired
    private UserRepository userRepository;

    @PostMapping("/register")
    public ResponseEntity<?> register(@RequestBody Map<String, String> requestBody) {
        String username = requestBody.get("username");
        String password = requestBody.get("password");

        if (username == null || username.isEmpty() || username.length() > 255) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Nom d'utilisateur invalide.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }

        if (password == null || password.isEmpty() || password.length() > 255) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Mot de passe invalide.");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(error);
        }

        User existingUser = userRepository.findByUsername(username);

        if (existingUser != null) {
            HashMap<String, String> error = new HashMap<>();
            error.put("error", "Le nom d'utilisateur existe déjà.");
            return ResponseEntity.status(HttpStatus.CONFLICT).body(error);
        }

        User newUser = new User();
        newUser.setUsername(username);
        newUser.setPassword(password); // Dans un vrai projet, pensez à hasher le mot de passe avant de le stocker
        newUser.setRole(UserRole.ROLE_USER);

        userRepository.save(newUser);

        Map<String, Object> userDetails = new HashMap<>();
        userDetails.put("username", newUser.getUsername());
        userDetails.put("role", newUser.getRole());

        return ResponseEntity.status(HttpStatus.CREATED).body(userDetails);
    }

}