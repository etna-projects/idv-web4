package com.quest.etna.dto;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Objects;

public class AuthenticateUserBodyDto {
    public AuthenticateUserBodyDto(String username, String password) {
        this.username = username;
        this.password = password;
    }

    @NotBlank(message = "Le nom d'utilisateur est obligatoire.")
    @Size(min = 1, max = 255, message = "Le nom d'utilisateur doit contenir entre 1 et 255 caractères.")
    private String username;

    @NotBlank(message = "Le mot de passe est obligatoire.")
    @Size(min = 1, max = 255, message = "Le mot de passe doit contenir entre 1 et 255 caractères.")
    private String password;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AuthenticateUserBodyDto)) return false;
        AuthenticateUserBodyDto that = (AuthenticateUserBodyDto) o;
        return Objects.equals(username, that.username) && Objects.equals(password, that.password);
    }

    @Override
    public int hashCode() {
        return Objects.hash(username, password);
    }

    @Override
    public String toString() {
        return "AuthenticateUserBody{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }
}
