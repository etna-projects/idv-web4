package com.quest.etna.controller;

import com.quest.etna.exceptions.HttpException;
import com.quest.etna.model.Address;
import com.quest.etna.model.JwtUserDetails;
import com.quest.etna.model.User;
import com.quest.etna.repositories.AddressRepository;
import com.quest.etna.dto.CreateAddressBodyDto;
import com.quest.etna.dto.UpdateAddressBodyDto;
import com.quest.etna.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolationException;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.HashMap;
import java.util.Objects;

@RestController
@RequestMapping("address")
@Validated
public class AddressController {
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private UserRepository userRepository;

    @GetMapping()
    public ResponseEntity<?> getAllAddresses(
            HttpServletRequest request
    ) {
       if(request.isUserInRole("ROLE_ADMIN")) {
           Iterable<Address> addresses = addressRepository.findAll();
           return ResponseEntity.ok(addresses);
       }
       Authentication auth = SecurityContextHolder.getContext().getAuthentication();
       JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
       User user = userRepository.findByUsername(jwtUserDetails.getUsername());
       return ResponseEntity.ok(user.getAddress());
    }

    @GetMapping("{id}")
    public ResponseEntity<?> getAddressById(
            HttpServletRequest request,
            @PathVariable("id") @Min(1) int id
    ) throws HttpException {
        Address address = addressRepository.findById(id).orElse(null);
        if (address == null) {
            throw HttpException.notFound("Address not found");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User user = userRepository.findByUsername(jwtUserDetails.getUsername());
        if (!Objects.equals(address.getUser().getId(), user.getId()) && !request.isUserInRole("ROLE_ADMIN")) {
            throw HttpException.forbidden("You are not allowed to access this address");
        }
        return ResponseEntity.ok(address);
    }

    @PostMapping("")
    public ResponseEntity<?> createAddress(
            @Valid @RequestBody CreateAddressBodyDto newAddress
    ) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User user = userRepository.findByUsername(jwtUserDetails.getUsername());
        Address address = new Address();
        address.setStreet(newAddress.getStreet());
        address.setCity(newAddress.getCity());
        address.setPostalCode(newAddress.getPostalCode());
        address.setCountry(newAddress.getCountry());
        address.setUser(user);
        addressRepository.save(address);
        return ResponseEntity.status(HttpStatus.CREATED).body(address);
    }

    @PutMapping("{id}")
    public ResponseEntity<?> updateAddressById(
            HttpServletRequest request,
            @PathVariable("id") @Min(1) int id,
            @Valid @RequestBody UpdateAddressBodyDto updatedAddress
    ) throws HttpException {
        Address address = addressRepository.findById(id).orElse(null);
        if (address == null) {
            throw HttpException.notFound("Address not found");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User user = userRepository.findByUsername(jwtUserDetails.getUsername());
        if (!Objects.equals(address.getUser().getId(), user.getId()) && !request.isUserInRole("ROLE_ADMIN")) {
            throw HttpException.forbidden("You are not allowed to delete this address");
        }
        // Since the updatedAddress fields are nullable, we only update the fields that are not null
        if (updatedAddress.getStreet() != null) {
            address.setStreet(updatedAddress.getStreet());
        }
        if (updatedAddress.getCity() != null) {
            address.setCity(updatedAddress.getCity());
        }
        if (updatedAddress.getPostalCode() != null) {
            address.setPostalCode(updatedAddress.getPostalCode());
        }
        if (updatedAddress.getCountry() != null) {
            address.setCountry(updatedAddress.getCountry());
        }
        addressRepository.save(address);
        return ResponseEntity.ok(address);
    }

    @DeleteMapping("{id}")
    public ResponseEntity<?> deleteAddressById(
            HttpServletRequest request,
            @PathVariable("id") @Min(1) int id
    ) throws HttpException {
        Address address = addressRepository.findById(id).orElse(null);
        if (address == null) {
            throw HttpException.notFound("Address not found");
        }
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        JwtUserDetails jwtUserDetails = (JwtUserDetails) auth.getPrincipal();
        User user = userRepository.findByUsername(jwtUserDetails.getUsername());
        if (!Objects.equals(address.getUser().getId(), user.getId()) && !request.isUserInRole("ROLE_ADMIN")) {
            throw HttpException.forbidden("You are not allowed to delete this address");
        }
        HashMap<String, Boolean> response = new HashMap<>();
        try {
            addressRepository.delete(address);
        } catch (Exception e) {
            response.put("success", false);
            return ResponseEntity.internalServerError().body(response);
        }
        response.put("success", true);
        return ResponseEntity.ok(response);
    }
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseEntity<?> handleConstraintViolationException(ConstraintViolationException e) {
        HashMap<String, String> errors = new HashMap<>();
        e.getConstraintViolations().forEach(constraintViolation -> {
            String propertyPath = constraintViolation.getPropertyPath().toString();
            String message = constraintViolation.getMessage();
            errors.put(propertyPath, message);
        });
        HashMap<String, HashMap<String, String>> response = new HashMap<>();
        response.put("errors", errors);
        return new ResponseEntity<>(response, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpException.class)
    ResponseEntity<?> handleException(HttpException e) {
        HashMap<String, String> response = new HashMap<>();
        response.put("error", e.getMessage());
        return new ResponseEntity<>(response, e.getStatus());
    }
}
