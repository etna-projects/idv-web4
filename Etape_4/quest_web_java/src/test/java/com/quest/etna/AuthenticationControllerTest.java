package com.quest.etna;

import com.quest.etna.model.User;
import com.quest.etna.repositories.UserRepository;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AuthenticationControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    private static final HashMap<String,String> testUser = new HashMap<String,String>(){{
        put("username", "test");
        put("password", "test");
    }};

    private String jwtToken;

    @BeforeAll
    public void setup() {
        User user = userRepository.findByUsername(testUser.get("username"));
        if (user != null) {
            userRepository.delete(user);
        }
    }

    @AfterAll
    public void cleanup() {
        User user = userRepository.findByUsername(testUser.get("username"));
        if (user != null) {
            userRepository.delete(user);
        }
    }

    @Test
    @Order(1)
    @DisplayName("La route /register répond bien en 201.")
    void registerShouldReturn201() throws Exception {
        this.mockMvc
                .perform(
                        post("/register")
                                .contentType("application/json")
                                .content(new JSONObject(testUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isCreated());
    }

    @Test
    @Order(2)
    @DisplayName("Si vous rappelez /register avec les mêmes paramètres, vous obtenez bien une réponse 409 car l'utilisateur existe déjà.")
    void registerShouldReturn409() throws Exception {
        this.mockMvc
                .perform(
                        post("/register")
                                .contentType("application/json")
                                .content(new JSONObject(testUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isConflict());
    }

    @Test
    @Order(3)
    @DisplayName("La route /authenticate retourne bien un statut 200 ainsi que votre token.")
    void authenticateShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        post("/authenticate")
                                .contentType("application/json")
                                .content(new JSONObject(testUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("token"));
        jwtToken = jsonResponse.getString("token");
    }

    @Test
    @Order(4)
    @DisplayName("La route /authenticate retourne bien un statut 401 si le mdp est mauvais.")
    void authenticateShouldReturn401() throws Exception {
        String oldPassword = testUser.get("password");
        testUser.put("password", "wrongPassword");
        String response = this.mockMvc
                .perform(
                        post("/authenticate")
                                .contentType("application/json")
                                .content(new JSONObject(testUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("error"));
        testUser.put("password", oldPassword);
    }

    @Test
    @Order(5)
    @DisplayName("La route /me retourne un statut 200 avec les informations de l'utilisateur (attention à bien penser au token Bearer).")
    void meShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        get("/me")
                                .contentType("application/json")
                                .accept("application/json")
                                .header("Authorization", "Bearer " + jwtToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("username"));
        assertEquals(testUser.get("username"), jsonResponse.getString("username"));
        assertFalse(jsonResponse.has("password"));
        assertTrue(jsonResponse.has("role"));
        assertEquals("ROLE_USER", jsonResponse.getString("role"));
    }
}
