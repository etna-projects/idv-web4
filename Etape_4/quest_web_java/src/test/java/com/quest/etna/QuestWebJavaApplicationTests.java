package com.quest.etna;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.platform.suite.api.SelectClasses;
import org.junit.platform.suite.api.Suite;
import org.springframework.test.context.junit.jupiter.SpringExtension;

@ExtendWith(SpringExtension.class)
@Suite
@SelectClasses({
        DefaultControllerTest.class,
        AuthenticationControllerTest.class,
        AddressControllerTest.class,
        UserControllerTest.class
})
class QuestWebJavaApplicationTests {


}
