package com.quest.etna;

import com.github.javafaker.Faker;
import com.quest.etna.model.Address;
import com.quest.etna.model.User;
import com.quest.etna.repositories.AddressRepository;
import com.quest.etna.repositories.UserRepository;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Locale;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@AutoConfigureMockMvc
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class AddressControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private AddressRepository addressRepository;

    static Faker faker = new Faker(new Locale("en-GB"));

    private static final HashMap<String,String> testUser = new HashMap<String,String>(){{
        put("username", "test");
        put("password", "test");
    }};

    private static final HashMap<String,String> testAddress = new HashMap<String,String>(){{
        put("street", faker.address().streetAddress());
        put("city", faker.address().city());
        put("postalCode", faker.address().zipCode());
        put("country", faker.address().country());
    }};

    private int addressId;

    private String jwtToken;

    @BeforeAll
    public void setup() throws Exception {
        User user = userRepository.findByUsername(testUser.get("username"));
        if (user != null) {
            userRepository.delete(user);
        }
        this.mockMvc
                .perform(
                        post("/register")
                                .contentType("application/json")
                                .content(new JSONObject(testUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isCreated());
        String response = this.mockMvc
                .perform(
                        post("/authenticate")
                                .contentType("application/json")
                                .content(new JSONObject(testUser).toString())
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("token"));
        jwtToken = jsonResponse.getString("token");
    }

    @AfterAll
    public void cleanup() {
        User user = userRepository.findByUsername(testUser.get("username"));
        if (user != null) {
            userRepository.delete(user);
        }
    }

    @Test
    @Order(1)
    @DisplayName("sans token Bearer, la route /address/ retourne bien un statut 401.")
    void getAddressShouldReturn401WithoutToken() throws Exception {
        this.mockMvc
                .perform(
                        get("/address")
                                .contentType("application/json")
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isUnauthorized());
    }

    @Test
    @Order(2)
    @DisplayName("avec un token Bearer valide, la route /address/ retourne bien un statut 200.")
    void getAddressShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        get("/address")
                                .contentType("application/json")
                                .header("Authorization", "Bearer " + jwtToken)
                                .accept("application/json")
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        // check if response is a valid JSON array
        JSONArray jsonResponse = new JSONArray(response);
        assertTrue(jsonResponse.length() >= 0);
        for(int i = 0; i < jsonResponse.length(); i++) {
            JSONObject address = jsonResponse.getJSONObject(i);
            assertTrue(address.has("id"));
            assertTrue(address.has("street"));
            assertTrue(address.has("city"));
            assertTrue(address.has("postalCode"));
            assertTrue(address.has("country"));
            assertFalse(address.has("user"));
        }
    }

    @Test
    @Order(3)
    @DisplayName("La route POST /address répond bien en 201.")
    void createAddressShouldReturn201() throws Exception {
        String response = this.mockMvc
                .perform(
                        post("/address")
                                .contentType("application/json")
                                .accept("application/json")
                                .header("Authorization", "Bearer " + jwtToken)
                                .content(new JSONObject(testAddress).toString())
                )
                .andDo(print())
                .andExpect(status().isCreated())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("id"));
        assertTrue(jsonResponse.has("street"));
        assertTrue(jsonResponse.has("city"));
        assertTrue(jsonResponse.has("postalCode"));
        assertTrue(jsonResponse.has("country"));
        assertEquals(testAddress.get("street"), jsonResponse.getString("street"));
        assertEquals(testAddress.get("city"), jsonResponse.getString("city"));
        assertEquals(testAddress.get("postalCode"), jsonResponse.getString("postalCode"));
        assertEquals(testAddress.get("country"), jsonResponse.getString("country"));
        // On vérifie que lq propriety user n'est pas renvoyée
        assertFalse(jsonResponse.has("user"));
        // On vérifie que l'adresse a bien été créée en base de données
        assertNotNull(addressRepository.findById(jsonResponse.getInt("id")));
        addressId = jsonResponse.getInt("id");
    }

    @Test
    @Order(4)
    @DisplayName("La route GET /address répond bien en 200 et retourne bien l'adresse demandée.")
    void getAddressByIdShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        get("/address/" + addressId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + jwtToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("id"));
        assertTrue(jsonResponse.has("street"));
        assertTrue(jsonResponse.has("city"));
        assertTrue(jsonResponse.has("postalCode"));
        assertTrue(jsonResponse.has("country"));
        assertEquals(testAddress.get("street"), jsonResponse.getString("street"));
        assertEquals(testAddress.get("city"), jsonResponse.getString("city"));
        assertEquals(testAddress.get("postalCode"), jsonResponse.getString("postalCode"));
        assertEquals(testAddress.get("country"), jsonResponse.getString("country"));
        // On vérifie que l'adresse existe en base de données
        assertNotNull(addressRepository.findById(jsonResponse.getInt("id")));
    }

    @Test
    @Order(5)
    @DisplayName("La route PUT /address répond bien en 200 et peut update juste une seule proprieté.")
    void updateAddressShouldReturn200() throws Exception {
        HashMap<String,String> updatedAddress = new HashMap<String,String>(){{
            put("street", "2 rue de la paix");
        }};
        String response = this.mockMvc
                .perform(
                        put("/address/" + addressId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + jwtToken)
                                .content(new JSONObject(updatedAddress).toString())
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("id"));
        assertTrue(jsonResponse.has("street"));
        assertTrue(jsonResponse.has("city"));
        assertTrue(jsonResponse.has("postalCode"));
        assertTrue(jsonResponse.has("country"));
        assertEquals(updatedAddress.get("street"), jsonResponse.getString("street"));
        assertEquals(testAddress.get("city"), jsonResponse.getString("city"));
        assertEquals(testAddress.get("postalCode"), jsonResponse.getString("postalCode"));
        assertEquals(testAddress.get("country"), jsonResponse.getString("country"));
        // On vérifie que l'adresse a bien été mise à jour en base de données
        Optional<Address> dbAddress = addressRepository.findById(jsonResponse.getInt("id"));
        assertTrue(dbAddress.isPresent());
        assertEquals(updatedAddress.get("street"), dbAddress.get().getStreet());
    }

    @Test
    @Order(6)
    @DisplayName("La route DELETE /address/{id} répond bien en 200 et supprime bien l'adresse.")
    void deleteAddressShouldReturn200() throws Exception {
        String response = this.mockMvc
                .perform(
                        delete("/address/" + addressId)
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + jwtToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        JSONObject jsonResponse = new JSONObject(response);
        assertTrue(jsonResponse.has("success"));
        if(jsonResponse.getBoolean("success")) {
            // On vérifie que l'adresse a bien été supprimée en base de données
            Optional<Address> dbAddress = addressRepository.findById(addressId);
            assertFalse(dbAddress.isPresent());
        } else {
            fail("La suppression de l'adresse a échoué.");
        }
    }

    @Test
    @Order(7)
    @DisplayName("La route GET /address répond bien en 200 et retourne bien les adresses sans user.")
    void getAddressShouldNotSendUserWithAdress() throws Exception {
        this.mockMvc
                .perform(
                        post("/address")
                                .contentType("application/json")
                                .accept("application/json")
                                .header("Authorization", "Bearer " + jwtToken)
                                .content(new JSONObject(testAddress).toString())
                )
                .andDo(print())
                .andExpect(status().isCreated());
        String response = this.mockMvc
                .perform(
                        get("/address")
                                .contentType(MediaType.APPLICATION_JSON)
                                .accept(MediaType.APPLICATION_JSON)
                                .header("Authorization", "Bearer " + jwtToken)
                )
                .andDo(print())
                .andExpect(status().isOk())
                .andReturn().getResponse().getContentAsString();
        assertNotNull(response);
        // check if the response is a json array
        JSONArray jsonResponse = new JSONArray(response);
        // check if the response is not empty
        assertTrue(jsonResponse.length() > 0);
        // check if the response contains the right properties
        JSONObject firstAddress = jsonResponse.getJSONObject(0);
        assertTrue(firstAddress.has("id"));
        assertTrue(firstAddress.has("street"));
        assertTrue(firstAddress.has("city"));
        assertTrue(firstAddress.has("postalCode"));
        assertTrue(firstAddress.has("country"));
        assertFalse(firstAddress.has("user"));
    }

}
